import { Component, OnInit } from '@angular/core';
import { RestApiService } from 'src/app/core/rest-api.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  public columnDefs = [
    { field: 'strCategory', headerName: 'Category' },
    { field: 'strAlcoholic', headerName: 'Alcoholic'},
    { field: 'strDrink', headerName: 'Drink'},
    { field: 'strIngredient1', headerName: 'Ingredient1'},
    { field: 'strIngredient2', headerName: 'Ingredient2'},
    { field: 'strIngredient3', headerName: 'Ingredient3'},
    { field: 'strIngredient4', headerName: 'Ingredient4'}
  ];

  public rowData = [];
  public filter = ''
  private gridApi: any;
  private gridColumnApi: any;
  public defaultColDef: any;

  constructor(
    private restApi: RestApiService
  ) { 
    this.defaultColDef = {
      // flex: 1,
      // minWidth: 150,
      filter: true,
      sortable: true
    };
  }

  ngOnInit(): void {
    
  }

  onGridReady(params: any) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.restApi.getDrinks().subscribe((result: any)=> {
      this.rowData = result[`drinks`];
    });
    
  }

  onFilterTextBoxChanged() {
    this.gridApi.setQuickFilter(this.filter);
  } 

}
