import { HttpClientModule } from '@angular/common/http';
import { HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { RestApiService } from './rest-api.service';


describe('RestApiService', () => {
    let service: RestApiService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [RestApiService]
        });
        service = TestBed.inject(RestApiService);

    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('be able to retrieve drinks from the API via GET', () => {
        // Arrange
        const dummyDrinks = {
            drinks: [{
                strAlcoholic: 'Alcoholic',
                strCategory: 'Shot',
                strIngredient1: '151 proof rum'
            }]
        }

        // Assert
        service.getDrinks().subscribe((result: any) => {
            expect(result.drinks.length).toBe(1);
            expect(result.drinks).toEqual(dummyDrinks);
        });

    });
});
