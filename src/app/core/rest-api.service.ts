import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RestApiService {

    constructor(private http: HttpClient) {
        
    }

    getDrinks() {
        return this.http.get('https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a')
    }
}