# Test

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

## Set up for running the project

Node.js, angular Cli needed to run the below project.

1) Download/clone project
2) Run `npm install`
3) Run `npm start` to run the project which will serve the project on localhost:4200
4) Run `npm test` to run test cases. which will create code coverage folder inside project. 
5) Go inside code coverage folder & open index.html with browser which will show code coverage report for the project.



